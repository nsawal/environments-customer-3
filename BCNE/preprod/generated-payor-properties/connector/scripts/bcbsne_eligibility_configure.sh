#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./bcbsne_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** BCBSNE SH : bcbsne_eligibility_configure.sh start on ${HOST_NAME} **********"

    echo "Start configuring eligibility job"

        eligibilityJsonConfigFile=$HE_DIR/bcbsne-eligibilityextract/resources/config/eligibility-job-config.json
        chmod -R 755 $eligibilityJsonConfigFile

        sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $eligibilityJsonConfigFile

        extractStartEndTime=$(date +"%Y-%m-%dT%T.%3NEDT")
        echo "extractStartEndTime: ${extractStartEndTime}"

        sed -i -e "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $eligibilityJsonConfigFile
        sed -i -e "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $eligibilityJsonConfigFile

        extractOutputPath=$(getProperty eligibility_extract_output_path)
        echo "extractOutputPath: ${extractOutputPath}"
        mkdir -p $extractOutputPath

        sed -i -e "s|#EXTRACT_OUTPUT_PATH#|${extractOutputPath}|g" $eligibilityJsonConfigFile
         
        configStagingDir=$(grep "extractConfigStagingDir" $HE_DIR/etc/com.healthedge.he.common.extract.configuration.cfg | sed -e "s/extractConfigStagingDir=//g")
        mkdir -p $configStagingDir

        extractConfigRequestDir=$(grep "extractConfigRequestDir" $HE_DIR/etc/com.healthedge.he.common.extract.configuration.cfg | sed -e "s/extractConfigRequestDir=//g")
        echo "extractConfigRequestDir: ${extractConfigRequestDir}"
        mkdir -p $extractConfigRequestDir

        jobName=$(grep "jobName" $eligibilityJsonConfigFile | sed -e "s/jobName//g;s/[\", :]//g")
        
        # "s/-/\./g"           - replace '-' with '.'
        # "s/\(.*\)/\L\1/g"    - convert to lower case
        configJobName=$(sed -e "s/-/\./g;s/\(.*\)/\L\1/g" <<< "$jobName")
        
        cfgFile=$HE_DIR/etc/com.healthedge.he.common.extract.${configJobName}.cfg
        if [ ! -f "$cfgFile" ]
        then
            echo "Dropping eligibility job configuration json ${eligibilityJsonConfigFile} to ${extractConfigRequestDir}"
            cp $eligibilityJsonConfigFile $extractConfigRequestDir/eligibility-job-config.json
        else
            echo "${jobName} is already configured"
        fi

    echo "End configuring eligibility extract job"


    echo "Start configuring eligibility trigger route"

        eligibilityJsonRouteFile=$HE_DIR/bcbsne-eligibilityextract/resources/config/eligibility-route-config.json
        chmod -R 755 $eligibilityJsonRouteFile

        extractCron=$(getProperty eligibility_extract_job_frequency)
        echo "extractCron: ${extractCron}"

        sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $eligibilityJsonRouteFile
        
        schedulerStagingDir=$(grep "stagingDir" $HE_DIR/etc/com.healthedge.he.common.scheduler.cfg | sed -e "s/stagingDir=//g")
        mkdir -p $schedulerStagingDir

        fileRequestBaseDir=$(grep "fileRequestBaseDir" $HE_DIR/etc/com.healthedge.he.common.scheduler.cfg | sed -e "s/fileRequestBaseDir=//g")
        fileRequestBaseDir=${fileRequestBaseDir}/request/cron

        mkdir -p $fileRequestBaseDir

        customBlueprint=$HE_DIR/custom-blueprints-deploy/generic-scheduler-cron-${jobName}.xml
        #if [ ! -f "$customBlueprint" ]
        #then
        echo "Dropping eligibility job trigger creation json ${eligibilityJsonRouteFile} to ${fileRequestBaseDir}"
        cp $eligibilityJsonRouteFile $fileRequestBaseDir/eligibility-route-config.json
        #else
        #    echo "Route for ${jobName} is already configured"
        #fi

    echo "End configuring eligibility job trigger route"


echo "********** BCBSNE SH : bcbsne_eligibility_configure.sh end on ${HOST_NAME} **********"
echo ""
