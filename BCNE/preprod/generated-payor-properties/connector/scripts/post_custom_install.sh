#!/bin/sh

####################################################################################################
# This script will be invoked and executed on Connector host by he-installer after installing Connector
# features and before invoking health check step.

#
# Make sure all the dependent files are present in this directory.
#
# The he-installer passes the following arguments in the same order to this script to use if needed
# $1 => HealthRulesConnectorServer Location
# $2 => Connector distribution location name
# $3 => Username being used to connect to HealthRulesConnectorServer
# $4 => Hostname

##################################################################################################

# Uncomment the following line to invoke providersearch script to copy and configure the buleprints and create indices in Elasticsearch
#./providersearch.sh $@
./hecommon_migration_custom_jobs.sh $1 $4
./bcbsne_hearbeatid_wss.sh $1 $4
./bcbsne_eligibility_configure.sh $1 $4
./bcbsne_account_configure.sh $1 $4
./bcbsne_eligibility_incremental_configure.sh $1 $4
./he_accumulator_configure.sh $1 $4
./bcbsne_bobcob_configure.sh $1 $4
./bcbsne_claim_payment_configure.sh $1 $4
./bcbsne_void_payment_configure.sh $1 $4
./bcbsne_alternate_address_configure.sh $1 $4
./removeUnusedCFGandBlueprintFiles.sh $1
./bcbsne_mam_configure.sh $1 $4