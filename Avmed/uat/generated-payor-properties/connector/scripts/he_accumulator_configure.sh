#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

echo ""
echo "********** GAC SH : he_accumulator_configure.sh start on ${HOST_NAME} **********"

    echo "Start configuring  he_accumulator_configure job"

        heAccumConfigFile=$HE_DIR/he-accumulator-outbound/resources/config/he-accumulator-config.json
        chmod -R 755 $heAccumConfigFile

        sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $heAccumConfigFile

        extractStartEndTime=$(date -d "yesterday" +"%Y-%m-%dT%T.%3NEDT")
        echo "extractStartEndTime: ${extractStartEndTime}"

        sed -i -e "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $heAccumConfigFile
        sed -i -e "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $heAccumConfigFile


        configStagingDir=$(grep "extractConfigStagingDir" $HE_DIR/etc/com.healthedge.he.common.extract.configuration.cfg | sed -e "s/extractConfigStagingDir=//g")
        echo "configStagingDir: ${configStagingDir}"
        mkdir -p $configStagingDir

        extractConfigRequestDir=$(grep "extractConfigRequestDir" $HE_DIR/etc/com.healthedge.he.common.extract.configuration.cfg | sed -e "s/extractConfigRequestDir=//g")
        echo "extractConfigRequestDir: ${extractConfigRequestDir}"
        mkdir -p $extractConfigRequestDir

        jobName=$(grep "jobName" $heAccumConfigFile | sed -e "s/jobName//g;s/[\", :]//g")
        
        # "s/-/\./g"           - replace '-' with '.'
        # "s/\(.*\)/\L\1/g"    - convert to lower case
        configJobName=$(sed -e "s/-/\./g;s/\(.*\)/\L\1/g" <<< "$jobName")
        
        cfgFile=$HE_DIR/etc/com.healthedge.he.common.extract.${configJobName}.cfg
        if [ ! -f "$cfgFile" ]
        then
            echo "Dropping he-accumulator job configuration json ${heAccumConfigFile} to ${extractConfigRequestDir}"
            cp $heAccumConfigFile $extractConfigRequestDir/he-accumulator-job-config.json
        else
            echo "${jobName} is already configured"
        fi

    echo "End configuring  he_accumulator_configure job"

echo "********** he_accumulator_configure.sh end on ${HOST_NAME} **********"
echo ""
